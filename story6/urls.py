from django.urls import re_path, path
from story6.views import *
from django.shortcuts import reverse
from django.views.generic import RedirectView

urlpatterns = [
    re_path(r'^$', RedirectView.as_view(pattern_name = 'story6:index')),
    re_path(r'^activities/$', index, name = 'index'),
    path('activities/add/', add_activity, name = 'add_activity'),
    path('activities/<int:code>/add_participant/', add_participant, name = 'add_participant'),
]