from django.test import TestCase, Client
from story6.models import Activity, Participant

class Story6Test(TestCase):
    def setUp(self):
        Activity.objects.create(title="Initial Testing")

    def test_index_exist(self):
        response = Client().get('/story-6/activities/')
        self.assertEqual(response.status_code, 200)

    def test_story6_exist(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().get('/story-6/', follow = True)
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)   

    def test_add_activity_post(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().post('/story-6/activities/add/', {'nama_kegiatan': 'Kegiatan Testing'}, follow = True)
        num_activity = Activity.objects.all().count()
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)
        self.assertEqual(num_activity, 2)
    
    def test_add_activity_post_invalid(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().post('/story-6/activities/add/', {'invalid_form': 'Kegiatan Testing'}, follow = True)
        num_activity = Activity.objects.all().count()
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)
        self.assertEqual(num_activity, 1)

    def test_add_activity_post_exceed_len(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().post('/story-6/activities/add/', {'nama_kegiatan': 'a' * 100}, follow = True)
        num_activity = Activity.objects.all().count()
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)
        self.assertEqual(num_activity, 1)

    def test_add_activity_get(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().get('/story-6/activities/add/', follow = True)
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)

    def test_add_participant_get_exist(self):
        response = Client().get('/story-6/activities/1/add_participant/')
        self.assertEqual(response.status_code, 200)
    
    def test_add_participant_get_not_exist(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().get('/story-6/activities/100000/add_participant/', follow = True)
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)

    def test_add_participant_post_exist(self):
        response = Client().post('/story-6/activities/1/add_participant/', {'nama_peserta': 'Bejo'})
        num_participant = Participant.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(num_participant, 1)
    
    def test_add_participant_post_exist_invalid_form(self):
        response = Client().post('/story-6/activities/1/add_participant/', {'invalid_form': 'Bejo'})
        num_participant = Participant.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(num_participant, 0)

    def test_add_participant_post_exist_exceed_len(self):
        response = Client().post('/story-6/activities/1/add_participant/', {'nama_peserta': 'a' * 100})
        num_participant = Participant.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(num_participant, 0)

    def test_add_participant_post_not_exist(self):
        REDIRECT_EXP = [('/story-6/activities/', 302)]
        response = Client().post('/story-6/activities/100000/add_participant/', {'nama_peserta': 'Bejo'}, follow = True)
        num_participant = Participant.objects.all().count()
        self.assertEquals(response.redirect_chain, REDIRECT_EXP)
        self.assertEqual(num_participant, 0)
    
    def test_template_index(self):
        response = Client().get('/story-6/activities/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_template_add_participants(self):
        response = Client().get('/story-6/activities/1/add_participant/')
        self.assertTemplateUsed(response, 'story6/add_participant.html')