from django.db import models

class Activity(models.Model):
    title = models.CharField(max_length = 50)

class Participant(models.Model):
    name = models.CharField(max_length = 50)
    activity = models.ForeignKey(Activity, on_delete = models.CASCADE)