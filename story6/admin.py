from django.contrib import admin
from story6.models import Activity, Participant

admin.site.register(Activity)
admin.site.register(Participant)
