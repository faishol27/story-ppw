from django.shortcuts import render, redirect, reverse
from story6.models import Activity, Participant

def index(req):
    context = {
        "activities": []
    }

    activities = Activity.objects.all()
    for activ in activities:
        peserta = Participant.objects.filter(activity = activ.pk)
        context['activities'].append({
            'title': activ.title,
            'id': activ.pk,
            'participants': peserta
        })
    return render(req, 'story6/index.html', context = context)

def add_activity(req):
    try:
        if req.method == "POST":
            title = req.POST['nama_kegiatan']
            if len(title) < 50:
                Activity.objects.create(title = title)
    finally:
        return redirect(reverse('story6:index'))
    
def add_participant(req, code):
    context = dict()
    try:
        context["activity"] = Activity.objects.get(pk = code) 
        try:
            if req.method == "POST":
                nama = req.POST['nama_peserta']
                if len(nama) < 50:
                    Participant.objects.create(name = nama, activity = Activity.objects.get(pk = code))
        finally:
            return render(req, 'story6/add_participant.html', context = context)
    except Activity.DoesNotExist:
        return redirect(reverse('story6:index'))