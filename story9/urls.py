from django.urls import re_path, path
from story9.views import *

urlpatterns = [
    re_path(r'^$', index, name = 'index'),
    path('login/', login_page, name = 'login'),
    path('logout/', logout_page, name = 'logout'),
    path('register/', register_page, name = 'register'),

]