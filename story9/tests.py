from django.test import TestCase, Client
from django.contrib.auth.models import User

class Story9Test(TestCase):
    client = Client()

    def test_story9_exist(self):
        RED_CHAIN = [('/story-9/login/', 302)]
        resp = Client().get('/story-9/', follow=True)
        self.assertEquals(RED_CHAIN, resp.redirect_chain)
    
    def test_story9_register_exist(self):
        resp = Client().get('/story-9/register/')
        self.assertEquals(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'story9/register.html')     

    def test_story9_register_valid_form(self):
        RED_CHAIN = [('/story-9/login/', 302)]
        resp = Client().post('/story-9/register/', {'username': '__test__', 'password1': 'IniPassword123', 'password2': 'IniPassword123'}, follow=True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)
        cntUser = User.objects.count()
        self.assertEqual(cntUser, 1)

    def test_story9_register_invalid_form(self):
        resp = Client().post('/story-9/register/', {'ini': 'invalid form'})
        self.assertEqual(resp.status_code, 200)
        cntUser = User.objects.count()
        self.assertEqual(cntUser, 0)

    def test_story9_login_exist(self):
        resp = Client().get('/story-9/login/')
        self.assertEquals(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'story9/login.html')

    def test_story9_login_valid_form(self):
        User.objects.create_user(username = '__test__', password = 'Halo1234')
        RED_CHAIN = [('/story-9/', 302)]
        resp = Client().post('/story-9/login/', {'username': '__test__', 'password': 'Halo1234'}, follow=True)
        self.assertEquals(RED_CHAIN, resp.redirect_chain)
        self.assertTemplateUsed(resp, 'story9/welcome.html')

    def test_story9_login_invalid_form(self):
        resp = Client().post('/story-9/login/', {'ini': 'invalid form'})
        self.assertEqual(resp.status_code, 200)
    
    def test_story9_logout_exist(self):
        RED_CHAIN = [('/story-9/login/', 302)]
        resp = Client().get('/story-9/logout/', follow=True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)

    def test_story9_login_authorized(self):
        User.objects.create_user(username = '__test__', password = 'Halo1234')
        self.client.login(username = '__test__', password = 'Halo1234')

        RED_CHAIN = [('/story-9/', 302)]
        resp = self.client.get('/story-9/login/', follow=True)
        self.assertEquals(RED_CHAIN, resp.redirect_chain)
    
    def test_story9_register_authorized(self):
        User.objects.create_user(username = '__test__', password = 'Halo1234')
        self.client.login(username = '__test__', password = 'Halo1234')

        RED_CHAIN = [('/story-9/', 302)]
        resp = self.client.get('/story-9/register/', follow=True)
        self.assertEquals(RED_CHAIN, resp.redirect_chain)