from django.shortcuts import render, reverse, redirect, resolve_url
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

def index(req):
    if req.user.is_authenticated:
        context = {
            "custom_menu": [ {"text": "Logout", "url": reverse('story9:logout')} ],
            "user": req.user
        }

        return render(req, 'story9/welcome.html', context)
    else:
        return redirect('story9:login')
    
def login_page(req):
    if not req.user.is_authenticated:
        loginForm = AuthenticationForm()
        if req.method == "POST":
            loginForm = AuthenticationForm(data=req.POST.copy())
            if loginForm.is_valid():
                user = loginForm.get_user()
                if user != None:
                    login(req, user)
                return redirect('story9:index')

        context = {
            "custom_menu": [
                {"text": "Login", "url": reverse('story9:login'), "active": True},
                {"text": "Register", "url": reverse('story9:register')},
            ],
            "form": loginForm
        }
        return render(req, 'story9/login.html', context)
    else:
        return redirect('story9:index')

def register_page(req):
    if not req.user.is_authenticated:
        registForm = UserCreationForm()
        if req.method == "POST":
            registForm = UserCreationForm(req.POST)
            if registForm.is_valid():
                user = registForm.save()
                return redirect('story9:login')
        
        context = {
            "custom_menu": [
                {"text": "Login", "url": reverse('story9:login')},
                {"text": "Register", "url": reverse('story9:register'), "active": True},
            ],
            "form": registForm
        }
        return render(req, 'story9/register.html', context)
    else:
        return redirect('story9:index')

def logout_page(req):
    logout(req)
    return redirect('story9:login')