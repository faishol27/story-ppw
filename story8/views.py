from django.shortcuts import render
from django.http import JsonResponse

import json
import requests

def index(req):
    return render(req, 'story8/index.html')

def list_buku(req):
    query = req.GET.get('q', '')
    resp_json = {'totalItems': 0}
    if query != '':
        resp = requests.get(f'https://www.googleapis.com/books/v1/volumes?q={query}')
        resp_json = json.loads(resp.content.decode())
    return JsonResponse(resp_json)