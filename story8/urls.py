from django.urls import re_path, path
from story8.views import *

urlpatterns = [
    re_path(r'^$', index, name = 'index'),
    path('list-buku/', list_buku, name = 'api'),
]