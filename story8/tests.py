from django.test import TestCase, Client
import json

class Story6Test(TestCase):
    def test_index_url(self):
        resp = Client().get('/story-8/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'story8/index.html')
    
    def test_list_buku_no_query(self):
        resp = Client().get('/story-8/list-buku/')
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, {'totalItems': 0})

    def test_list_buku_query(self):
        resp = Client().get('/story-8/list-buku/?q=ilmu')
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertNotEquals(resp_json, {'totalItems': 0})
