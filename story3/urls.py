from django.urls import re_path
from story3.views import index, send

urlpatterns = [
    re_path('^$', index, name = 'index'),
    re_path('^send', send, name='send'),
]
