from django.shortcuts import render

context = {
    "custom_menu": [
        {"text": "about me", "url": "/story-3/#about-me"},
        {"text": "projects", "url": "/story-3/#projects"},
        {"text": "blogs", "url": "/story-3/#blogs"},
        {"text": "contact me", "url": "/story-3/#contact-me"},
    ]
}

def index(request):
    return render(request, 'story3/index.html', context)

def send(request):
    return render(request, 'story3/send.html', context)