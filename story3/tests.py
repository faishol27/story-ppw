from django.test import TestCase, Client

class Story3Test(TestCase):
    def test_story3_exist(self):
        response = Client().get('/story-3/')
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, 'story3/index.html')

    def test_story3_send_exist(self):
        response = Client().get('/story-3/send/')
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, 'story3/send.html')