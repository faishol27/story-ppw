from django import forms

class CourseForm(forms.Form):
    nama_mata_kuliah = forms.CharField(max_length=100)
    jumlah_sks = forms.IntegerField()
    dosen = forms.CharField(max_length=100, widget=forms.Textarea)
    semester_tahun = forms.CharField(max_length=50)
    ruang_kelas = forms.CharField(max_length=50)
    deskripsi = forms.CharField(widget=forms.Textarea, required=False)
