from django.db import models

class Course(models.Model):
    name = models.CharField(max_length = 100)
    lecturer = models.CharField(max_length = 100)
    sks = models.IntegerField()
    description = models.TextField(blank=True)
    term = models.CharField(max_length=50)
    room = models.CharField(max_length=50)
