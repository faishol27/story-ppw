from django.shortcuts import render, redirect
from story5.models import Course
from story5.forms import CourseForm

def index(request):
    context = {
        'courses': Course.objects.all(),
        'form': CourseForm()
    }
    return render(request, 'story5/index.html', context)

def view_course(request, courseid):
    try:
        context = {
            'course': Course.objects.get(id = courseid)
        }
        return render(request, 'story5/detail_course.html', context)
    except:
        return redirect('/story-5/')

def add_course(request):
    if request.method == "POST":
        form = CourseForm(request.POST)
        if form.is_valid():
            new_course = Course()
            new_course.name = form.cleaned_data['nama_mata_kuliah']
            new_course.lecturer = form.cleaned_data['dosen']
            new_course.sks = form.cleaned_data['jumlah_sks']
            new_course.description = form.cleaned_data['deskripsi']
            new_course.term = form.cleaned_data['semester_tahun']
            new_course.room = form.cleaned_data['ruang_kelas']
            new_course.save()
    return redirect('/story-5/')

def del_course(request, courseid):
    Course.objects.filter(id = courseid).delete()
    return redirect('/story-5/')