from django.test import TestCase, Client
from story5.models import Course

class Story5Test(TestCase):
    def setUp(self):
        Course.objects.create(name = 'Test Nama', lecturer = 'Test Dosen', sks = 3,
            description = 'Test description', term = '2020/2021', room = 'AG-21')
    
    def test_story5_exist(self):
        response = Client().get('/story-5/')
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, 'story5/index.html')

    def test_delete_not_exist(self):
        RED_CHAIN = [('/story-5/', 302)]
        response = Client().get('/story-5/1000/delete/', follow = True)
        self.assertEquals(response.redirect_chain, RED_CHAIN)
        
        cntObj = Course.objects.all().count()
        self.assertEqual(cntObj, 1)
    
    def test_delete_exist(self):
        RED_CHAIN = [('/story-5/', 302)]
        response = Client().get('/story-5/1/delete/', follow = True)
        self.assertEquals(response.redirect_chain, RED_CHAIN)
        
        cntObj = Course.objects.all().count()
        self.assertEqual(cntObj, 0)
    
    def test_view_not_exist(self):
        RED_CHAIN = [('/story-5/', 302)]
        response = Client().get('/story-5/1000/view/', follow = True)
        self.assertEquals(response.redirect_chain, RED_CHAIN)
    
    def test_view_exist(self):
        response = Client().get('/story-5/1/view/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story5/detail_course.html')
    
    def test_add_get(self):
        RED_CHAIN = [('/story-5/', 302)]
        response = Client().get('/story-5/add/', follow = True)
        self.assertEquals(response.redirect_chain, RED_CHAIN)

    def test_add_post_valid(self):
        data = {
            'nama_mata_kuliah': 'Test 2',
            'dosen': 'Test 2 - Dosen',
            'jumlah_sks': '2',
            'deskripsi': 'Test 2 - Deskripsi',
            'semester_tahun': 'Ganjil 2020/2021',
            'ruang_kelas': 'ABC'
        }
        RED_CHAIN = [('/story-5/', 302)]
        response = Client().post('/story-5/add/', data = data, follow = True)
        self.assertEquals(response.redirect_chain, RED_CHAIN)

        cntObj = Course.objects.all().count()
        self.assertEqual(cntObj, 2)

    def test_add_post_invalid(self):
        RED_CHAIN = [('/story-5/', 302)]
        response = Client().post('/story-5/add/', data = {'its_just': 'invalid_form'}, follow = True)
        self.assertEquals(response.redirect_chain, RED_CHAIN)

        cntObj = Course.objects.all().count()
        self.assertEqual(cntObj, 1)
