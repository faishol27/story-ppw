from django.urls import re_path, path
from story5.views import index, view_course, add_course, del_course

urlpatterns = [
    re_path('^$', index, name = 'index'),
    path('<int:courseid>/view/', view_course),
    path('add/', add_course),
    path('<int:courseid>/delete/', del_course)
]
