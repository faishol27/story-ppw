"""story_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path, re_path
from django.shortcuts import reverse
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^story-1/', include(('story1.urls', 'story1'), namespace='story1')),
    url(r'^story-3/', include(('story3.urls', 'story3'), namespace='story3')),
    url(r'^story-5/', include(('story5.urls', 'story5'), namespace='story5')),
    url(r'^story-6/', include(('story6.urls', 'story6'), namespace='story6')),
    url(r'^story-7/', include(('story7.urls', 'story7'), namespace='story7')),
    url(r'^story-8/', include(('story8.urls', 'story8'), namespace='story8')),
    url(r'^story-9/', include(('story9.urls', 'story9'), namespace='story9')),
    re_path(r'^$', RedirectView.as_view(pattern_name='story9:index')),
]
