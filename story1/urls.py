from django.urls import re_path
from story1.views import index

urlpatterns = [
    re_path('^$', index, name='index'),
]
