from django.test import TestCase, Client

class Story1Test(TestCase):
    def test_story1_exist(self):
        response = Client().get('/story-1/')
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, 'story1/index.html')