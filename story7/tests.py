from django.test import TestCase, Client

class Story3Test(TestCase):
    def test_story7_exist(self):
        response = Client().get('/story-7/')
        self.assertEqual(response.status_code, 200) 
        self.assertTemplateUsed(response, 'story7/index.html')