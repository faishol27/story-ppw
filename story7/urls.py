from django.urls import re_path, path
from story7.views import *
from django.shortcuts import reverse
from django.views.generic import RedirectView

urlpatterns = [
    re_path(r'^$', index, name = 'index'),
]