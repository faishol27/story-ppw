from django.shortcuts import render

def index(req):
    context = {
        'contents': [
            ('Aktivitas Saat Ini', 
                'Saat ini saya sibuk berkuliah dan menjadi asisten dosen untuk mata kuliah dasar-dasar pemrograman 1.'),
            ('Pengalaman Kerja',
                """Saya memiliki cukup banyak pengalaman kerja pada bidang mengajar. Beberapa kali saya diminta untuk mengajar materi
                kompetisi sains nasional bidang komputer untuk tingkat kabupaten dan provinsi. Saya juga sempat diminta untuk menjadi
                pembuat materi serta soal untuk pembinaan kegiatan terkait."""),
            ('Pengalaman Organisasi', 
                """Organisasi yang saya ikuti saat ini adalah Ristek CSUI. Pada organisasi tersebut, saya terdaftar sebagai junior
                member dari network security dam operating system SIG."""),
            ('Pengalaman Kepanitiaan', 
                """Selama diperkuliahan, saya tergabung dalam beberapa kepanitiaan. Saya menjadi technical committee CPC dan scientific
                committee CTF pada COMPFEST 12. Selain itu, saya juga tergabung dalam staff ForEx (Fordel Expo) yakni sebuah acara try
                out dan expo UI yang diadakan oleh paguyuban daerah Fordel UI."""),
            ('Prestasi', 
                """Beberapa prestasi yang saya dapatkan hingga saat ini adalah juara 1 pada kompetisi CTF Pekan Ristek 2019. Selain itu,
                saya dan tim berhasil menjadi finalis pada beberapa lomba CTF seperti Gemastik 13, CyberJawara 2020, dan Hology 3."""),
        ]
    }
    return render(req, 'story7/index.html', context)